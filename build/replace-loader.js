/**
 * Created by wangjianbing on 2017/5/6.
 */
"use strict";

var loaderUtils = require('loader-utils')

module.exports = function(content) {
  this.cacheable && this.cacheable();
  var query = loaderUtils.getOptions(this) || {};
  if (!query.replace) {
    return content;
  }
  if (Array.isArray(query.replace)) {
    query.replace.forEach(replace => {
      content = content.replace(replace.from, replace.to);
    })
  }else if(typeof query.replace === 'object'){
    content = content.replace(query.replace.from, query.replace.to);
  }
  return content;
};
