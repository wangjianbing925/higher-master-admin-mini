/*
* wx
*/
import { baseApiUrl } from 'config'
import _ from 'lodash'

export default {
  init: function () {
    const request = wx.request;
    Object.keys(wx).forEach(key => {
      if (typeof wx[ key ] === 'function' && !/Sync$/.test(key) && !/createAnimation/.test(key)
        && !/^on/.test(key) && !/^start/.test(key) && !/^getWifiList/.test(key) && !/^create/.test(key)) {
        const tempFunc = wx[ key ];
        Object.defineProperty(wx, key, {
          value(config) {
            config = config || {};
            return new Promise(function (resolve, reject) {
              const success = config.success;
              config.success = function (result) {
                console.info(result)
                resolve(result)
                success && success(result);
              }
              const fail = config.fail;
              config.fail = function (result) {
                console.info(result)
                resolve({ code: 500, data: result, msg: JSON.stringify(result) })
                fail && fail({ code: 500, data: result, msg: JSON.stringify(result) })
              }
              tempFunc(config)
            });
          },
          writable: true
        })
      }
    })
    const showLoading = wx.showLoading;
    Object.defineProperty(wx, 'showLoading', {
      value(config) {
        if (_.isObject(config)) {
          showLoading(config)
        } else {
          showLoading({
            title: config || '加载中...',
          })
        }
      }
    })
    const showToast = wx.showToast;
    Object.defineProperty(wx, 'showToast', {
      value(config) {
        if (_.isObject(config)) {
          showToast(config)
        } else {
          showToast({
            title: config,
            icon: 'none',
            duration: 3000,
          })
        }
      }
    })
    Object.defineProperty(wx, 'request', {
      async value(config) {
        config = Object.assign({
          url: baseApiUrl,
          header: { 'content-type': 'application/x-www-form-urlencoded;charset=UTF-8' },
        }, config);
        if (!/^http/.test(config.url)) {
          config.url = baseApiUrl + config.url;
        }
        if (/higher-master.com/.test(config.url)) {
          if ((!App.loginInfo || !App.loginInfo.token) && !/emp\/login\/login\/wxCode/.test(config.url)) {
            await App.login();
          }
          config.header = Object.assign(config.header || {}, {'x-emp-token': (App.loginInfo && App.loginInfo.token) || ''})
        }
        return new Promise(function (resolve) {
          const success = config.success;
          config.success = async function (result) {
            if (result.data && result.data.code === 30014 && !/emp\/login\/login\/wxCode/.test(config.url)) {
              const pages = getCurrentPages();
              const pageId = pages.pop().route;
              pageId !== 'pages/mobile-bind/main' && wx.navigateTo({
                url: '/pages/mobile-bind/main'
              })
              return false;
            }
            console.info(result.data)
            resolve(result.data)
            success && success(result);
          }
          const fail = config.fail;
          config.fail = function (result) {
            resolve({ code: 500, data: result, msg: JSON.stringify(result) })
            fail && fail(result)
          }
          request(config)
        });
      },
      writable: true
    })
  }
}
