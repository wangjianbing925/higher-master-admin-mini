/*
*
*/
export default status => {
  switch (status) {
  case 0:
    return '未支付'
  case 1:
    return '待财务审核'
  case 2:
    return '已付定金'
  case 3:
    return '已支付'
  case 4:
    return '待退款'
  case 5:
    return '已退款'
  case 6:
    return '无需退款'
  }
}
