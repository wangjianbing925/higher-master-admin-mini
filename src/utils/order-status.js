/*
*
*/
export default status => {
  switch (status) {
  case 0:
    return '待支付'
  case 1:
    return '待审核'
  case 2:
    return '待财务审核'
  case 3:
    return '待制定训练计划'
  case 4:
    return '训练中'
  case 5:
    return '未发货'
  case 6:
    return '已发货'
  case 7:
    return '已收货'
  case 8:
    return '已完成'
  case 9:
    return '已评价'
  case 10:
    return '已取消'
  case 11:
    return '暂不发货'
  case 12:
    return '退件'
  }
}
