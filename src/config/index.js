/**
 * 小程序配置文件
 */
export default {
  //助高业务域名
  baseApiUrl: "API_DOMAIN",
  //客户业务后台请求域名
  serverUrl: "https://room.qcloud.com",
  //腾讯云RoomService后台请求域名
  roomServiceUrl: "https://room.qcloud.com",
  webrtcServerUrl: "https://xzb.qcloud.com/webrtc/weapp/webrtc_room",
  orderStatusList: [
    { id: 0, name: "待支付" },
    { id: 1, name: "待审核" },
    { id: 2, name: "待财务审核" },
    { id: 3, name: "待制定训练计划" },
    { id: 4, name: "训练中" },
    { id: 5, name: "未发货" },
    { id: 6, name: "已发货" },
    { id: 7, name: "已收货" },
    { id: 8, name: "已完成" },
    { id: 9, name: "已评价" },
    { id: 10, name: "已取消" }
  ],
  userTagList: [
    {
      id: 2,
      name: "普通用户（未报价）"
    },
    {
      id: 9,
      name: "普通用户（已报价）"
    },
    {
      id: 10,
      name: "普通用户（不说话）"
    },
    {
      id: 11,
      name: "普通用户（删除/拉黑）"
    },
    {
      id: 1,
      name: "意向用户"
    },
    {
      id: 8,
      name: "已成单用户"
    },
    {
      id: 12,
      name: "复购用户"
    },
    {
      id: 3,
      name: "失联用户"
    },
    {
      id: 4,
      name: "投诉用户"
    },
    {
      id: 5,
      name: "风险用户"
    },
    {
      id: 6,
      name: "黑名单用户"
    }
  ],
  userTagLevelList: [
    {
      id: 1,
      name: "一星"
    },
    {
      id: 2,
      name: "两星"
    },
    {
      id: 3,
      name: "三星"
    },{
      id: 4,
      name: "四星"
    },{
      id: 5,
      name: "五星"
    },]
};
